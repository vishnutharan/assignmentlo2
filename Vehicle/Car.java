package Vehicle;

public class Car extends FuelVehicle {
private int nbseats;

public Car (double basefee, double nbkms, double nbseats) {
	super (basefee, nbkms);
	this.nbseats = (int)nbseats;
}

public int getNbSeats() {
	return nbseats;
}
	
	public void setNbSeats(int nbseats) {
		this.nbseats = nbseats;
	}
	@ Override
	
	public double getMilageFees()	{
		return super.getMilageFees();
	}
	
	public double getCost() {
		return (nbseats * getBaseFee()) + getMilageFees();
	}
}
