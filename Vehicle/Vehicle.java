package Vehicle;

import java.util.Scanner;

public class Vehicle {
public static void main(String[] args) {
	
	Scanner sc = new Scanner (System.in);
	System.out.println("Welcome To The Vehicle Rentel Company");
	
	  String[] rentedvehicle = {"1.Car", "2.Truck", "3.Bicycle"};
	  
	  for (String i : rentedvehicle) {
	      System.out.println(i);
	    }  
	  
	  System.out.println("Plese Enter The Vehicle ?");
	  int Choice = sc.nextInt();
	  
	  switch(Choice) {
	  	  case 1:
		  Car car = new Car (500,250,Choice);					
		  car.setNbSeats (5);
			System.out.println("You Are Selecting The Benz Car");
			System.out.println("Number Of Seats: "+ car.getNbSeats());
			System.out.println("Cost Of Rent Per Day:" + "Rs." + car.getCost());
			break;
			
	  case 2: 
		  Truck truck = new Truck (50,10);
		  truck.setCapacity (1000);
			System.out.println("You Are Selecting The Tata Truck");
			System.out.println("Capacity  Of Truck is : "+ truck.getCapacity());
			System.out.println("Cost Of Rent Per Day:" + "Rs. " + truck.getCost());
			break;
			
	  case 3: 
		  Bicycle bicycle = new Bicycle (10,30);											// 10 is Base Fee and 30 is number of days
			System.out.println("You Are Selecting The Race Bicycle");
			System.out.println("Number Of Days for Rent : "+ bicycle.getNbDays());		
			System.out.println("Cost Of Rent Per Day:" + "Rs." + bicycle.getCost());
			break;
			
	  				}
			}
	}