package Vehicle;

public class RentedVehicle {

	private double basefee;
	
	public RentedVehicle (double basefee)	{
		super();
		this.basefee = basefee;
	}
	public double getBaseFee() {
		return basefee;
		
	}
	public void setBasefee(double basefee)	{
		this.basefee = basefee;
	}
}
