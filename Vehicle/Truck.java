package Vehicle;

public class Truck extends FuelVehicle {
	private int capacity;

	public Truck(double basefee, double nbkms) {
		super(basefee, nbkms);
			}

	@Override
	public double getMilageFees() {
	return super.getMilageFees();
					}
	
	public void setCapacity (int capacity) {
	this.capacity = capacity;
				}
	
	public int getCapacity() {
	return capacity;
			}
	
	public double getCost() {
		return (capacity * getBaseFee())+ getMilageFees();
			}
	
	}
